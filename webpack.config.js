var webpack = require('webpack');
var entry = {
        projects: './src/app/projects.js',
        registration: './src/app/registration.js',
        summary: './src/app/summary.js'
    },
    output = {
        path: __dirname,
        filename: "[name].entry.js"
    },
    uglifyJsPlugin = new webpack.optimize.UglifyJsPlugin({
        compressor: {
            screw_ie8: true,
            warnings: false
        },
        output: {
            comments: false
        }
    });

module.exports.development = {
    debug: true,
    devtool: 'eval',
    entry: entry,
    output: output,
    module: {
        loaders: [
            {test: /\.js?$/, exclude: /node_modules/, loader: 'babel-loader'},
            {test: /\.hbs$/, loader: 'handlebars-loader'},
            {test: /\.html$/, loader: "html-loader"}
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            apiUrl: JSON.stringify("http://localhost:8080"),
            ENV: JSON.stringify("dev")
        })
    ]
};

module.exports.production = {
    debug: false,
    entry: entry,
    output: output,
    module: {
        loaders: [
            {test: /\.js?$/, exclude: /node_modules/, loader: 'babel-loader'},
            {test: /\.hbs$/, loader: 'handlebars-loader'},
            {test: /\.html$/, loader: "html-loader"}
        ]
    },
    plugins: [
        uglifyJsPlugin,
        new webpack.DefinePlugin({
            apiUrl: JSON.stringify(""),
            ENV: JSON.stringify("prod")
        })
    ]
};
