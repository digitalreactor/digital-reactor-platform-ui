import * as Backbone from "backbone";

export default Backbone.Model.extend({
    url: apiUrl + "/registration/account",
    defaults: {
        counterId: "",
        email: "",
        sessionId: "",
        name: ""
    }
});
