import * as Backbone from "backbone";

export default Backbone.Model.extend({
    defaults: {
        counterId: "",
        name: ""
    }
});