import * as Backbone from "backbone";

export default Backbone.Model.extend({
    url: null,
    defaults: {
        status: "UNKNOWN",
        date: ""
    },
    initialize: function (options) {
        this.url = apiUrl + "/api/projects/" + options.id + "/summaries/last";
    }
});