import * as Backbone from "backbone";

export default Backbone.Model.extend({
    url: null,
    initialize: function (options) {
        this.url = apiUrl + "/api/summaries/" + options.id;
    }
});