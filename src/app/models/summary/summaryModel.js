import * as Backbone from "backbone";

export default Backbone.Model.extend({
    url: apiUrl + "/summaries/taskId/",
    initialize: function (options) {
        if (options && options.hasOwnProperty("summaryTaskId")) {
            this.url += options.summaryId;
        }
    }
});