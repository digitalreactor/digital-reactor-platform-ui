import $ from 'jquery'
import * as Backbone from 'backbone';
import SummaryComponent from './views/summary/summary.component';

var Router = Backbone.Router.extend({
    routes: {
        'id/:summaryId': 'summary'
    },
    summary: function (summaryId) {
        var summaryComponent = new SummaryComponent({summaryId: summaryId});
        $("#sites-app").html(summaryComponent.render().el);
    }
});

new Router();
Backbone.history.start();
