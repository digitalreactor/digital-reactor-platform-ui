import $ from 'jquery';
import ProjectComponent from './views/projects/projects.component';

var projectComponent = new ProjectComponent();

$("#projects-app").html(projectComponent.render().el);
