import RegistrationComponent from "./views/registration/registration.component"
import * as Backbone from 'backbone'
import $ from 'jquery'

var registrationComponent = new RegistrationComponent();
$("#registration-app").html(registrationComponent.render().el);

var Router = Backbone.Router.extend({
    routes: {
        '': 'index',
        'access': 'access',
        'sites/session/:sessionId': 'sites',
        'success': 'success'
    },
    index: function () {
        registrationComponent.goToStep(registrationComponent.STEPS.INITIAL);
    },
    access: function () {
        registrationComponent.goToStep(registrationComponent.STEPS.ACCESS);
    },
    sites: function (sessionId) {
        registrationComponent.goToStep(registrationComponent.STEPS.SITES, {sessionId: sessionId});
    },
    success: function () {
        registrationComponent.goToStep(registrationComponent.STEPS.SUCCESS);
    }
});

new Router();
Backbone.history.start();