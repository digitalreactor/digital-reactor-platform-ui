import * as Backbone from "backbone";
import SiteModel from "../../models/sites/siteModel";

export default Backbone.Collection.extend({
    url: apiUrl + "/accounts/sites",
    model: SiteModel
});