import * as Backbone from "backbone";
import ProjectModel from "../../models/project/projectModel"

export default Backbone.Collection.extend({
    url: apiUrl + '/api/projects/',
    model: ProjectModel,
    initialize: function (options) {
    }
});