import * as Backbone from "backbone";
import CounterModel from "../../models/registration/counterModel"

//models/registration/counterModel
export default Backbone.Collection.extend({
    baseUrl: apiUrl + '/registration/counters/',
    url: "",
    model: CounterModel,
    initialize: function (options) {
        this.url = this.baseUrl;

        if (options && options.hasOwnProperty("sessionId")) {
            this.url += options.sessionId;
        }
    }
});