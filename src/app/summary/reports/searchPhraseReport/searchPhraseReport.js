import * as Backbone from 'backbone';
import _ from 'underscore'
import SearchPhraseReport from './searchPhraseReport.hbs';
import $ from 'jquery'
import Marionette from 'backbone.marionette';

export default Marionette.ItemView.extend({
    template: SearchPhraseReport,
    reports: null,
    goals: [],
    currentGoal: 0,
    events: {
        "change #directSearchPhrasesSelector": "directSearchPhrasesSelector"
    },
    initialize: function (options) {
        this.reports = options.model;
        var self = this;
        //Todo[St.maxim] hack!
        this.goals = [];
        this.reports.forEach(function (model, index) {
            self.goals.push({
                goalName: model.goalName,
                type: model.type,
                index: index,
                chosen: index == 0
            });
        });
    },
    directSearchPhrasesSelector: function () {
        //Todo[St.maxim] hack!
        this.goals[this.currentGoal].chosen = false;
        var selector = $('#directSearchPhrasesSelector').find(":selected");
        this.currentGoal = selector.val();
        this.goals[this.currentGoal].chosen = true;
        this.render();

    },
    serializeData() {
        return {
            goals: this.goals,
            successPhrases: this.reports[this.currentGoal].successPhrases,
            badPhrases: this.reports[this.currentGoal].badPhrases
        };
    }
});
