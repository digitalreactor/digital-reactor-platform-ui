import * as Backbone from 'backbone';
import _ from 'underscore'
import $ from 'jquery'
import AbstractDifferentSamplingReport from '../common/abstractDifferentSamplingReport'
import SearchPhraseReport from './searchPhraseReport'

export default AbstractDifferentSamplingReport.extend({

    reportRender: function (reportMode) {
        var searchPhraseReport = new SearchPhraseReport({model: reportMode.get("reports")});

        return searchPhraseReport.render().el;
    },
    reportName: function () {
        return "Поисковые фразы Яндекс Директа";
    }
});