import * as Backbone from 'backbone';
import _ from 'underscore'
import ReferringSourceReport from './referringSourceReport.hbs';
import $ from 'jquery'
import Marionette from 'backbone.marionette';

export default Marionette.ItemView.extend({
    template: ReferringSourceReport,
    referringSourceReport: null,
    currentReferringSources: null,
    goals: [],
    currentGoal: 0,
    events: {
        "change #referringSourceSelector": "referringSourceSelector"
    },
    initialize: function (options) {
        this.referringSourceReport = options.model;
        this.currentReferringSources = options.model.get("sources");
        var self = this;
        this.goals = [{
            goalName: "Общая статистика",
            index: 0,
            chosen: true
        }];
        this.referringSourceReport.get("sourcesWithGoals").forEach(function (model, index) {
            self.goals.push({
                goalName: model.name,
                index: index + 1,
                chosen: false
            });
        });
    },
    referringSourceSelector: function () {
        var selector = $('#referringSourceSelector').find(":selected");
        this.currentGoal = selector.val();
        if (this.currentGoal == 0) {
            this.currentReferringSources = this.referringSourceReport.get("sources");
        } else {
            this.currentReferringSources = this.referringSourceReport
                .get("sourcesWithGoals")[this.currentGoal - 1]["sources"];
        }

        this.render();
    },
    serializeData() {
        return {
            referringSources: this.currentReferringSources,
            goals: this.goals
            /*      goals: this.goals,
             successPhrases: this.reports[this.currentGoal].successPhrases,
             badPhrases: this.reports[this.currentGoal].badPhrases*/
        };
    },
    onRender() {
        this.chart();
    },
    chart: function () {
        var self = this;

        var options = {
            dataType: "script",
            cache: true,
            url: "https://www.gstatic.com/charts/loader.js"
        };
        $.ajax(options).done(function(){
            google.charts.load('current', {'packages': ['line', 'corechart']});
            google.charts.setOnLoadCallback(drawReferringSourceChart);
        });


        function drawReferringSourceChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string');

            for (var j = 0; j < self.currentReferringSources.length; j++) {
                data.addColumn('number', self.currentReferringSources[j].name);
            }


            var chartDots = [];


            for (var i = 0; i < self.currentReferringSources[0].metrics.length; i++) {
                var row = [];
                row.push(self.currentReferringSources[0].metrics[i].date);
                for (j = 0; j < self.currentReferringSources.length; j++) {
                    row.push(self.currentReferringSources[j].metrics[i].number);
                }

                chartDots.push(row);
            }

            data.addRows(chartDots);

            var options = {
                chart: {
                    title: 'Иточники трафика'
                },
                interpolateNulls: true,
                width: 900,
                height: 300
            };

            var chart = new google.charts.Line(document.getElementById("referring-source-chart"));

            chart.draw(data, options);
        }

    }
});
