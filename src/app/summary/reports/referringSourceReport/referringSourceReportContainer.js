import * as Backbone from 'backbone';
import _ from 'underscore'
import $ from 'jquery'
import AbstractDifferentSamplingReport from '../common/abstractDifferentSamplingReport'
import ReferringSourceReport from './referringSourceReport'

export default AbstractDifferentSamplingReport.extend({

    reportRender: function (reportMode) {
        var referringSourceReport = new ReferringSourceReport({model: reportMode});

        return referringSourceReport.render().el;
    },
    reportName: function () {
        return "Источники переходов на сайт";
    }
});