import * as Backbone from 'backbone';
import ReportViewResolver from './reportViewResolver'

export default Backbone.View.extend({
    reportViewResolver: ReportViewResolver,
    update: function (availableReports) {
        var self = this;
        availableReports.forEach(function (reportModel) {
            var ReportView = self.reportViewResolver.create(reportModel.reportType);
            if (ReportView) {
                self.$el.append(new ReportView({reportIds: reportModel.reportIds}).render().el);
            } else {
                console.log("Handler for report type: " + reportModel.reportType + " not found.")
            }
        });
    },
    render: function () {
        return this;
    }
});