import * as Backbone from 'backbone';
import LoaderBar from '../../../utils/LoaderBar'
import ReportModel from './reportModel'
import AbstractTemplate from './abstractDifferentSamplingReport.html';
import _ from 'underscore'
import $ from 'jquery'

/**
 * This class implements a pattern "Template method" with next steps:
 * 1. Load all models.
 * 2.
 * 3.
 */

export default Backbone.View.extend({
    initialize: function (options) {
        var countOfLoadedReports = 0;
        var fetchedModels = [];
        var renderedReports = [];

        var self = this;
        var ReportModel = this.reportModel();
        options.reportIds.forEach(function (reportId) {
            countOfLoadedReports++;
            var reportModel = new ReportModel({id: reportId});
            reportModel.fetch({
                success: function () {
                    fetchedModels.push(reportModel);
                    if (fetchedModels.length == countOfLoadedReports) {
                        self.$el.html(_.template(AbstractTemplate)({
                            reportName: self.reportName()
                        }));

                        var reportContent = self.$el.find(".reportContent");

                        var samplingSelector = self.$el.find(".samplingSelector");
                        $.each(fetchedModels, function (i, report) {
                            samplingSelector.append(new Option(report.get("samplingInterval"), i));
                        });

                        samplingSelector.change(function (value) {
                            var currentSelectedSampling = samplingSelector.find(":selected").val();

                            if (!renderedReports[currentSelectedSampling]) {
                                renderedReports[currentSelectedSampling] = self.reportRender(fetchedModels[currentSelectedSampling]);
                            }

                            reportContent.html(renderedReports[currentSelectedSampling]);
                        });

                        renderedReports[0] = self.reportRender(fetchedModels[0]);
                        reportContent.html(renderedReports[0]);
                    }
                },
                error: function () {
                    console.log("Report wasn't loaded.");
                    countOfLoadedReports--;
                }
            })
        });
    },
    reportRender: function (reportMode) {
        console.error("Method doesn't implement.");
    },
    reportName: function () {
        return "Abstract"
    },
    reportModel: function () {
        return ReportModel;
    },
    render: function () {
        this.$el.html(LoaderBar.html());

        return this;
    }
});