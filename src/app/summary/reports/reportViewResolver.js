import VisitsForTimeIntervalReportContainer from './visitsForTimeIntervalReport/visitsForTimeIntervalReportContainer';
import PagesWithHighFailureRateReportContainer from './pagesWithHighFailureRateReport/pagesWithHighFailureRateReportContainer'
import SearchPhraseReportContainer from './searchPhraseReport/searchPhraseReportContainer'
import ReferringSourceReportContainer from './referringSourceReport/referringSourceReportContainer'
var registry = {
    "VisitsForTimeIntervalReport": VisitsForTimeIntervalReportContainer,
    "PagesWithHighFailureRateReport": PagesWithHighFailureRateReportContainer,
    "SearchPhraseReport": SearchPhraseReportContainer,
    "ReferringSourceReport": ReferringSourceReportContainer
};

export default {
    create: function (reportType) {
        return registry[reportType];
    }
};
