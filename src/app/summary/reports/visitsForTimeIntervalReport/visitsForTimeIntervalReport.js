import * as Backbone from 'backbone';
import _ from 'underscore'
import VisitsForTimeIntervalReport from './visitsForTimeIntervalReport.html';
import $ from 'jquery'

export default Backbone.View.extend({
    data: null,
    initialize: function (options) {
        this.data = options.model;
    },
    render: function () {
        var self = this;
        this.$el.html(_.template(VisitsForTimeIntervalReport)({
            description: self.__description(),
            reason: self.data.get("reason")
        }));
        this.__chart();

        return this;
    },
    __description: function () {

        var percent = this.data.get("percent").toFixed(2);
        switch (this.data.get("trend")) {
            case 'INCREASING':
            {
                return "Посещаемость увеличилась на " + percent + "% (" + this.data.get("visitChange") + " визитов)."
            }
            case 'DECREASING':
            {
                return "Посещаемость уменьшилась на " + percent + "% (" + this.data.get("visitChange") + " визитов)."
            }
            case 'UNALTERED':
            {
                return "Посещаемость не изменилась: " + this.data.get("visit") + " визитов."
            }
        }
    },
    __chart: function () {
        var self = this;

        var options = {
            dataType: "script",
            cache: true,
            url: "https://www.gstatic.com/charts/loader.js"
        };
        $.ajax(options).done(function () {
            google.charts.load('current', {'packages': ['line', 'corechart']});
            google.charts.setOnLoadCallback(drawChart);
        });


        function drawChart() {
            var ar = [];

            for (var i = 0; i < self.data.get("metrics").length; i++) {

                ar.push({
                    c: [
                        {v: self.data.get("metrics")[i].date},
                        {v: self.data.get("metrics")[i].number},
                        {v: self.data.get("metrics")[i].dayType == "HOLIDAY" ? 'color: #578EBE' : 'color: #5C9BD1'}
                    ]
                });
            }


            var dt = new google.visualization.DataTable({
                cols: [{id: 'task', label: 'Task', type: 'string'},
                    {id: 'hours', label: 'Визитов', type: 'number'},
                    {type: 'string', role: 'style'}
                ],
                rows: ar
            }, 0.6);

            var view = new google.visualization.DataView(dt);

            var options = {
                interpolateNulls: true,
                title: "Визиты за 30 дней",
                titleTextStyle: {
                    bold: false
                },
                chartArea: {
                    left: 50
                },
                width: 900,
                height: 300,
                bar: {groupWidth: "95%"}
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("visitsDuringMonthReport"));
            chart.draw(view, options);
        }
    }
});