import * as Backbone from 'backbone';
import _ from 'underscore'
import $ from 'jquery'
import AbstractDifferentSamplingReport from '../common/abstractDifferentSamplingReport'
import VisitsForTimeIntervalReport from './visitsForTimeIntervalReport'

export default AbstractDifferentSamplingReport.extend({
    reportRender: function (reportMode) {
        var visitsForTimeIntervalReport = new VisitsForTimeIntervalReport({model: reportMode});
        
        return visitsForTimeIntervalReport.render().el;
    },
    reportName: function () {
        return "Визиты на сайт";
    }
});