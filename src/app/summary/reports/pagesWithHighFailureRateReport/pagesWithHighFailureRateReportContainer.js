import * as Backbone from 'backbone';
import _ from 'underscore'
import $ from 'jquery'
import AbstractDifferentSamplingReport from '../common/abstractDifferentSamplingReport'
import PagesWithHighFailureRateReport from './pagesWithHighFailureRateReport'

export default AbstractDifferentSamplingReport.extend({
    reportRender: function (reportMode) {
        var pagesWithHighFailureRateReport = new PagesWithHighFailureRateReport({model: reportMode});

        return pagesWithHighFailureRateReport.render().el;
    },
    reportName: function () {
        return "Страницы с высоким показателем отказа";
    }
});