import * as Backbone from 'backbone';
import _ from 'underscore'
import PagesWithHighFailureRateReport from './pagesWithHighFailureRateReport.hbs';
import $ from 'jquery'
import Marionette from 'backbone.marionette';

export default Marionette.ItemView.extend({
    template: PagesWithHighFailureRateReport,
    model: null,
    initialize: function (options) {
        this.model = options.model;
    },
    serializeData() {
        return {
            pages: this.model.get("metrics")
        };
    }
});
