import InfoMessageTemplate from '../../templates/registration/info-message.html';
import _ from 'underscore'
import * as Backbone from "backbone";
import $ from 'jquery'

var YANDEX_OAUTH_URL = "https://oauth.yandex.ru";
var YANDEX_OAUTH_RESOURCE = "/authorize?response_type=code&state=NEW_USER&client_id=";
export default Backbone.View.extend({
    applicationId: '',
    events: {
        'click #go-to-site-step': "goToSitesStep"
    },
    initialize: function () {
    },
    render: function () {
        this.$el.html(_.template(InfoMessageTemplate)({}));

        return this;
    },
    goToSitesStep: function () {
        $.get("/configuration/application/id", function (applicationId) {
            if (applicationId == 'dev') {
                YANDEX_OAUTH_URL = "http://localhost:8080"
            }
            
            window.location = YANDEX_OAUTH_URL + YANDEX_OAUTH_RESOURCE + applicationId;
        });

    }
});

   