import * as Backbone from "backbone";
import _ from 'underscore'
import SuccessTemplate from '../../templates/registration/success-message.html'

export default Backbone.View.extend({
    events: {
        'click #go-to-site-step': "goToSitesStep"
    },
    render: function () {
        this.$el.html(_.template(SuccessTemplate)({}));

        return this;
    },
    goToSitesStep: function () {
        Backbone.history.navigate('sites', {trigger: true});

    }
});