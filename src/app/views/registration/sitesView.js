import _ from 'underscore'
import * as Backbone from 'backbone'
import SitesTemplate from '../../templates/registration/accessible-project.html';
import CountersCollection from '../../collections/registration/countersCollection';
import NewAccountModel from '../../models/registration/newAccountModel';
import $ from 'jquery'
import Radio from "backbone.radio"

var errorChannel = Radio.channel('errors');

export default Backbone.View.extend({
    currentSessionId: "",
    events: {
        'click #save': "save"
    },
    initialize: function () {

    },
    render: function (param) {
        this.$el.html(_.template(SitesTemplate)({}));
        this.currentSessionId = param.sessionId;
        var self = this;
        var counters = new CountersCollection({
            sessionId: param.sessionId
        });

        counters.fetch({
            success: function () {
                self.__selectorRender(counters);
            },
            error: function () {
                errorChannel.trigger('error', {
                    title: "Ошибка загрузки данных от Yandex.Metrika",
                    message: "Повторите попытку ввода или обратитесь в службу поддержки."
                });
            }
        });

        return this;
    },
    save: function () {
        this.$("#project-selector").addClass("hidden");
        this.$(".loading").removeClass("hidden");

        var email = localStorage.getItem('registration-email');
        var selector = $('#project-selector').find(":selected");
        var counterId = selector.val();
        var name = selector.text();

        var newAccount = new NewAccountModel({
            email: email,
            counterId: counterId,
            sessionId: this.currentSessionId,
            name: name
        });

        newAccount.save(null, {
            error: function () {
                errorChannel.trigger('error', {
                    title: "Ошибка при регистрации проекта",
                    message: "Повторите попытку ввода или обратитесь в службу поддержки."
                });
            },
            success: function () {
                Backbone.history.navigate('success', {trigger: true});
            }
        });
    },
    __selectorRender: function (counters) {
        var list = $("#project-list");
        counters.forEach(function (counter) {
            list.append(new Option(counter.get("name"), counter.get("counterId")));
        });

        this.$("#save").removeClass("disabled");
        this.$("#project-selector").removeClass("hidden");
        this.$(".loading").addClass("hidden");
    }
});
    