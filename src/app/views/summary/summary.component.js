import * as Backbone from 'backbone';
import AvailableReportsModel from '../../models/reports/availableReportsModel'
import ReportsListView from '../../summary/reports/reportsListView'

export default Backbone.View.extend({
    reportListView: new ReportsListView(),
    initialize: function (options) {
        var self = this;
        var availableReports = new AvailableReportsModel({
            id: options.summaryId
        });

        availableReports.fetch({
            success: function () {
                self.reportListView.update(availableReports.get('availableReports'));
            },
            error: function () {

            }
        });

        /*summaryModel.fetch({
         success: function () {
         summaryModel.get('reports').forEach(function (reportModel) {
         self.__reportRender(new Backbone.Model(reportModel));
         });
         },
         error: function () {

         }
         });*/
    },
    render: function () {
        this.$el.html(this.reportListView.render().el);

        return this;
    }/*,
    __reportRender: function (reportModel) {
        var ReportView = ReportViewFactory.create(reportModel);
        var reportView = new ReportView({
            data: reportModel
        });

        this.$el.append(reportView.render().el);
    }*/
});