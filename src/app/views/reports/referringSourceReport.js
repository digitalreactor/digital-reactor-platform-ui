import * as Backbone from 'backbone';
import _ from 'underscore'
import ReferringSourceReport from '../../templates/reports/referringSourceReport.html';
import ReferringSourceTable from '../../views/reports/rsr/referringSourceTable';
import GoalSelector from '../../views/reports/rsr/goalSelector';
import InfoAboutChange from '../../views/reports/rsr/infoAboutChange';
import Radio from "backbone.radio";
import $ from 'jquery';

var referringSourceReportChannel = Radio.channel('referringSourceReportChannel');

export default Backbone.View.extend({
    data: null,
    goalSelector: null,
    sourceTable: null,
    sourceInfo: null,
    withConversion: false,
    initialize: function (options) {
        this.data = options.data;
        if (this.data.get("sourcesWithGoals") && this.data.get("sourcesWithGoals").length) {
            this.goalSelector = new GoalSelector({
                goals: this.data.get("sourcesWithGoals")
            });
            this.withConversion = true;
        }

        this.__initSubComponent(0);

        referringSourceReportChannel.on("changeGoalSelector", this.__changeGoal, this);
    },
    render: function () {
        this.$el.html(_.template(ReferringSourceReport)({}));

        if (this.goalSelector) {
            var referringSourceSelector = this.$el.find("#referringSourceSelector");
            referringSourceSelector.html(this.goalSelector.render().el);
        }

        this.chart();

        this.__dataRender();

        return this;
    },
    __dataRender: function () {
        var referringSourceTable = this.$el.find("#referringSourceTable");
        referringSourceTable.html(this.sourceTable.render().el);

        var referringSourceInfo = this.$el.find("#referringSourceInfo");
        referringSourceInfo.html(this.sourceInfo.render().el);
    },
    __changeGoal: function (context) {
        var goalIndex = this.__findGoalIndex(context.currentGoal);
        this.__initSubComponent(goalIndex);

        this.__dataRender();
    },
    __findGoalIndex: function (name) {
        var index = 0;
        $.each(this.data.get("sourcesWithGoals"), function (i, value) {
            if (value.name === name) {
                index = i;
                return;
            }
        });

        return index;
    },
    __initSubComponent: function (indexGoal) {

        var sources = this.data.get("sources");
        var infoConstructorData = {
            withConversion: this.withConversion
        };

        if (this.withConversion) {
            sources = this.data.get("sourcesWithGoals")[indexGoal].sources;
            infoConstructorData.conversion = this.data.get("sourcesWithGoals")[indexGoal].conversion;
            infoConstructorData.conversionChange = this.data.get("sourcesWithGoals")[indexGoal].conversionChange;
            infoConstructorData.numberOfCompletedGoal = this.data.get("sourcesWithGoals")[indexGoal].numberOfCompletedGoal;
        }

        this.sourceTable = new ReferringSourceTable({
            withConversion: this.withConversion,
            sources: sources
        });

        this.sourceInfo = new InfoAboutChange(infoConstructorData);
    },
    chart: function () {
        var self = this;

        var options = {
            dataType: "script",
            cache: true,
            url: "https://www.gstatic.com/charts/loader.js"
        };
        $.ajax(options).done(function(){
            google.charts.load('current', {'packages': ['line', 'corechart']});
            google.charts.setOnLoadCallback(drawReferringSourceChart);
        });


        function drawReferringSourceChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string');

            for (var j = 0; j < self.data.get("sources").length; j++) {
                data.addColumn('number', self.data.get("sources")[j].name);
            }


            var chartDots = [];


            for (var i = 0; i < self.data.get("sources")[0].metrics.length; i++) {
                var row = [];
                row.push(self.data.get("sources")[0].metrics[i].date);
                for (j = 0; j < self.data.get("sources").length; j++) {
                    row.push(self.data.get("sources")[j].metrics[i].number);
                }

                chartDots.push(row);
            }

            data.addRows(chartDots);

            var options = {
                chart: {
                    title: 'Иточники трафика'
                },
                interpolateNulls: true,
                width: 900,
                height: 300
            };

            var chart = new google.charts.Line(document.getElementById("referring-source-chart"));

            chart.draw(data, options);
        }

    }
});
   