import _ from 'underscore'
import GoalSelector from '../../../templates/reports/rsr/goalSelector.html';
import * as Backbone from "backbone";
import Radio from "backbone.radio"
import $ from 'jquery'

var referringSourceReportChannel = Radio.channel('referringSourceReportChannel');

export default Backbone.View.extend({
    name: null,
    goals: null,
    events: {
        "change #goalSelectorReferringSources": "__changeGoalSelector"
    },
    initialize: function (options) {
        this.goals = options.goals;
    },
    render: function () {
        this.$el.html(_.template(GoalSelector)({}));
        var goalSelector = this.$el.find("#goalSelectorReferringSources");
        $.each(this.goals, function (i, goal) {
            goalSelector.append(new Option(goal.name, goal.name));
        });

        return this;
    },
    __changeGoalSelector: function () {
        var selector = $('#goalSelectorReferringSources').find(":selected");

        referringSourceReportChannel.trigger('changeGoalSelector', {
            currentGoal: selector.val()
        });
    }
});