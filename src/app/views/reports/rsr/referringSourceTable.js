import * as Backbone from 'backbone'
import $ from 'jquery'
import _ from 'underscore'
import Table from '../../../templates/reports/rsr/table.html';
import ConversionTable from '../../../templates/reports/rsr/conversionTable.html';
import Row from '../../../templates/reports/rsr/row.html';
import ConversionRow from '../../../templates/reports/rsr/conversionRow.html';

export default Backbone.View.extend({
    sources: null,
    withConversion: false,
    initialize: function (options) {
        this.sources = options.sources;
        this.withConversion = options.withConversion;
    },
    render: function () {
        var self = this;
        var currentTable = Table;
        var currentRow = Row;

        if (this.withConversion) {
            currentTable = ConversionTable;
            currentRow = ConversionRow;
        }

        this.$el.html(_.template(currentTable)({}));
        $.each(this.sources, function (i, source) {
            self.$el.find("tbody").append(_.template(currentRow)(self.__makeRowForTemplate(source)));
        });

        return this;
    },
    __makeRowForTemplate: function (source) {
        var objectViewData = {
            "name": source.name,
            "visits": source.totalVisits,
            "visitsChange": this.__resolveChangeForView(source.totalVisitsChangePercent)
        };

        if (this.withConversion) {
            objectViewData.goalVisits = source.totalGoalVisits;
            objectViewData.goalVisitsChange = this.__resolveChangeForView(source.totalGoalVisitsChangePercent);
            objectViewData.conversion = source.conversion;
            objectViewData.conversionChange = this.__resolveChangeForView(source.conversionChangePercent);
        }

        return objectViewData;

    },
    __resolveChangeForView: function (percent) {
        if (percent === 0.0) {
            return "";
        }
        var sign = percent > 0.0 ? "+" : "";

        return "( " + sign + " " + percent + "% )"
    }
});
