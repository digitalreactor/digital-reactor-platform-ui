import * as Backbone from "backbone";
import Radio from "backbone.radio"
import _ from 'underscore'
import ErrorTemplate from "../templates/error.html";

var errorChannel = Radio.channel('errors');

export default Backbone.View.extend({
    initialize: function () {
        errorChannel.on("error", this.errorHandler, this);
        errorChannel.on("success", this.errorHideHandler, this);
    },
    ref: function () {
        return this;
    },
    render: function (title, message) {
        this.$el.html(_.template(ErrorTemplate)({
            title: title,
            message: message
        }));

        return this;
    },
    errorHandler: function (context) {
        this.render(context.title, context.message);
    },
    errorHideHandler: function () {
        this.$el.empty();
    }
});