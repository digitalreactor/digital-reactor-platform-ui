import * as Backbone from "backbone";
import _ from 'underscore'
import ProjectStatusTemplate from './project-status.html';
import ProjectStatusModel from '../../models/project/projectStatusModel';
import $ from 'jquery'

export default Backbone.View.extend({
    site: '',
    id: '',
    events: {
        'click .reload': "__reload"
    },
    initialize: function (options) {
        this.id = options.id;
        this.__loadingData(this);
    },
    __loadingData: function (self) {
        var projectStatus = new ProjectStatusModel({id: this.id});

        projectStatus.fetch({
            success: function () {
                self.__statusRender(projectStatus);
            },
            error: function () {
                //TODO
            }
        });
    },
    render: function () {
        return this;
    },
    __statusRender: function (status) {
        var self = this;
        this.$el.html(_.template(ProjectStatusTemplate)({
            summaryId: status.get('id'),
            date: status.get("date") ? status.get("date") : ""
        }));

        this.$el.find(".loading").hide();
        switch (status.get('status')) {
            case "NEW": {
                this.$el.find(".awaiting").show();
                setTimeout(function () {
                    self.__loadingData(self);
                }, 2000);
                break;
            }
            case "COMPLETED": {
                this.$el.find(".project-selector").show();
                break;
            }
            case "RUNNING": {
                this.$el.find(".loading").show();
                setTimeout(function () {
                    self.__loadingData(self);
                }, 2000);
                break;
            }
        }
    },
    __reload: function () {
        var self = this;
        $.ajax({
            type: 'POST',
            url: apiUrl + "/api/projects/" + this.id + "/summaries",
            contentType: 'application/json',
            dataType: 'json',
            success: function (statusResult) {
                statusResult.id = self.id;
                self.__statusRender(new SummaryStatusModel(statusResult));
            },
            fail: function () {
                //TODO fail implement
            }
        });
    }
});
