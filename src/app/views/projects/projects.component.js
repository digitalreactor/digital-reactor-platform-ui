import * as Backbone from 'backbone';
import _ from 'underscore'
import Radio from "backbone.radio"
import ProjectsCollection from "../../collections/projects/projectsCollection";
import ProjectView from "./projectView"
import ComponentTemplate from "./projects-component.html"
import $ from 'jquery';

var errorChannel = Radio.channel('errors');

export default Backbone.View.extend({
    initialize: function () {
        var projectsCollection = new ProjectsCollection();
        var self = this;
        projectsCollection.fetch({
            success: function () {
                self.$el.find(".loading").hide();
                self.__projectsRender(projectsCollection);
            },
            error: function () {
                errorChannel.trigger('error', {
                    title: "Ошибка получения списка доступных сайтов",
                    message: "Обновите страницу или обратитесь в службу поддержки."
                });
            }
        });
    },
    render: function () {
        this.$el.html(_.template(ComponentTemplate)({}));

        return this;
    },
    __projectsRender: function (projects) {
        var self = this;
        projects.forEach(function (project) {
            var site = new ProjectView({model: project});
            self.$("#projects-list").append(site.render().el);
        });

    }
});
 
