import * as Backbone from "backbone";
import _ from 'underscore'
import ProjectTemplate from './project.html';
import ProjectStatusView from './projectStatusView';

export default Backbone.View.extend({
    model: null,
    initialize: function (options) {
        this.model = options.model;
    },
    render: function () {
        var statusView = new ProjectStatusView({id: this.model.get("id")});
        this.$el.html(_.template(ProjectTemplate)({
            name: this.model.get("name")
        }));
        this.$el.find(".status-box").html(statusView.render().el);

        return this;
    }
});